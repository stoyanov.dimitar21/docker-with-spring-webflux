# Docker With Spring Webflux

<img src="arch.png"/>

## Dockerizing Spring Application
* Microservice Development with Spring WebFlux

* Reactive Mongo

* Packaging Spring application as Docker Image

* Multi Stage Dockerfile

## Integration Testing With Test Container

* Improving the quality of application by writing integration tests using Docker

## Mockserver
* Starting early development process when the dependent service is not ready in Microservices architecture
* Writing Integration tests along with Mockserver

## API Gateway with Nginx
* Using Nginx for Path based routing and Load balancing

* Nginx + Microservices + Docker Compose to simulate mini cloud in local for testing

<p>Project made from a Udemy course: <a href="https://www.udemy.com/course/docker-java/" target="_blank">Docker From Scratch [For Spring Developers]</a>.
